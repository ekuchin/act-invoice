import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    contractor: {},
    customer: {},
    product: {}
  },
  mutations: {
    SET_CONTRACTOR(state, payload) {
      state.contractor = payload;
    },

    SET_CUSTOMER(state, payload) {
      state.customer = payload;
    },

    SET_PRODUCT(state, payload) {
      state.product = payload;
    }
  },
  actions: {
    setContractor(context, payload) {
      context.commit("SET_CONTRACTOR", payload);
    },
    setCustomer(context, payload) {
      context.commit("SET_CUSTOMER", payload);
    },
    setProduct(context, payload) {
      context.commit("SET_PRODUCT", payload);
    }
  },
  getters: {
    getContractor: state => {
      return state.contractor;
    },
    getCustomer: state => {
      return state.customer;
    },
    getProduct: state => {
      return state.product;
    },
    getSumm: state => {
      return state.product.price * state.product.quantity;
    }
  },

  modules: {}
});

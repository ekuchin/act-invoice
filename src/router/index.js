import Vue from "vue";
import VueRouter from "vue-router";
import Contractor from "../views/Contractor.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Contractor",
    component: Contractor
  },
  {
    path: "/customer",
    name: "Customer",
    component: () =>
      import(/* webpackChunkName: "customer" */ "../views/Customer.vue")
  },
  {
    path: "/product",
    name: "Product",
    component: () =>
      import(/* webpackChunkName: "product" */ "../views/Product.vue")
  },
  {
    path: "/act",
    name: "Act",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "act" */ "../views/Act.vue")
  },
  {
    path: "/invoice",
    name: "Invoice",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "invoice" */ "../views/Invoice.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
